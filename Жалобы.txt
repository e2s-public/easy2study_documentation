--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/report/?filter=new - список новых жалоб;
https://easy2study.com/war/api/clicks/report/?filter=processed - список обработанных жалоб;
Метод GET;
Требуется токен в header;
*Пользователь видит только свои жалобы;
*Администратор все;
Расширенно:
https://easy2study.com/war/api/clicks/report/?filter=new&type=user - список новых жалоб на пользователей;
https://easy2study.com/war/api/clicks/report/?filter=new&type=comment - список новых жалоб на комментарий;
https://easy2study.com/war/api/clicks/report/?filter=new&type=course - список новых жалоб на курс;
https://easy2study.com/war/api/clicks/report/?filter=new&type=videocourse - список новых жалоб на видеокурс;
https://easy2study.com/war/api/clicks/report/?filter=processed&type=user - список обработанных жалоб на пользователей;
https://easy2study.com/war/api/clicks/report/?filter=processed&type=comment - список обработанных жалоб на комментарий;
https://easy2study.com/war/api/clicks/report/?filter=processed&type=videocourse - список обработанных жалоб на видеокурс;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/report/ - создание жалобы;
Метод POST;
FormData:
Если жалоба на комментарий:
    data.append('text', 'Text here'); #текст
    data.append('screenshot', fs.createReadStream('/D:/Женя/monsters_45.jpg')); #скрин, если нужно
    data.append('comment', '5'); #id комментария
Если жалоба на пользователя:
    data.append('text', 'Text here'); #текст
    data.append('screenshot', fs.createReadStream('/D:/Женя/monsters_45.jpg')); #скрин, если нужно
    data.append('user', '124'); #id пользователя
Если жалоба на курс:
    data.append('text', 'Text here'); #текст
    data.append('screenshot', fs.createReadStream('/D:/Женя/monsters_45.jpg')); #скрин, если нужно
    data.append('course', '5'); #id курса
Если жалоба на видеокурс:
    data.append('text', 'Text here'); #текст
    data.append('screenshot', fs.createReadStream('/D:/Женя/monsters_45.jpg')); #скрин, если нужно
    data.append('videocourse', '5'); #id видеокурса
Требуется токен в header;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/report/{id}/ - просмотр жалобы;
Метод GET;
Body:
Требуется токен в header;
*Достуно автору либо администратору;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/report/{id}/ - ответ на жалобу;
Метод PUT;
Body:
{
    "answer": "Text",
    "status": 1
}
Требуется токен администратора в header;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/report/{id}/ - удалить жалобу;
Метод DELETE;
Требуется токен администратора в header;
--------------------------------------------------------------------------------------