--------------------------------------------------------------------------------------
https://easy2study.com/war/api/videocourse/object/{id} - просмотр видеокурса;
метод GET;
Из данного запроса достаём поле "review", которое даст аргументы для заполнения шапки страницы отзывов:
...
"review": {
        "mark": 4.0, #средняя оценка, если приходит None, то не отображаем круг и звёзды (пусто)
        "sum": "3 reviews", #общее количество
        "five": "1 reviews", #количество комментов с 5-ю звёздами, поля ниже аналогично
        "four": "1 reviews",
        "three": "1 reviews",
        "two": "0 reviews",
        "one": "0 reviews",
        "star_img": "https://easy2study.com/war/media/images/star.svg" #иконка звезды, может пригодиться
	"ratings": [
            {
                "rating": 1
            },
            {
                "rating": 1
            },
            {
                "rating": 1
            },
            {
                "rating": 0
            },
            {
                "rating": 0
            }
        ]
    },
...
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/videocourse/review/{id} - просмотр отзывов на видеокурс, по умолчанию сортировка от новых к старым;
Метод GET;
Сортировка:
https://easy2study.com/war/api/videocourse/review/{id}/?sort=5star - отзывы с 5-ю звёздами;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=4star - отзывы с 4-мя звёздами;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=3star - отзывы с 3-мя звёздами;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=2star - отзывы с 2-мя звёздами;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=1star - отзывы с 1-ой звёздой;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=fromlatest - от новых к старым либо с последнего;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=fromoldest - от старых к новым;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=popularity - по количеству лайков;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=positive - от положительных;
https://easy2study.com/war/api/videocourse/review/{id}/?sort=negative - от отрицательных;
Вместо {id} ставим id видеокурса, например 3;
*Для корректного отображения статуса "inlikes" требуется токен текущего пользователя;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/videocourse/review/ - создание отзыва;
Метод POST;
На каждый видеокурс только один отзыв для предотвращения спама;
Body:
{
    "videocourse_id": 3, #id видеокурса, на которого пишем
    "text": "Text here", #текст тут
    "rate": 4 #оценка, целое число от 1 до 5
}
Если вы преподаватель видеокурса, то вы можете оставить один ответ (не более, специально ограничил) на отзыв в вашем профиле, тело в таком случае отличается и представлено ниже;
Body:
{
    "videocourse_id": 20,
    "parent_id": 3, #id отзыва, на которого хотим написать ответ
    "text": "Text here", #текст тут
}
Требуется токен в header;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/videocourse/review/{id}/ - удаление отзыва на видеокурс (только администратор);
Метод DELETE;
Вместо {id} ставим id отзыва, например 10;
Требуется токен администратора в header;
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/clicks/likecomment/ - сделать или убрать лайк отзыва;
Метод POST;
Body:
{
    "comment_id": 10 #id отзыва, который лайкаем
}
Требуется токен в header;
--------------------------------------------------------------------------------------