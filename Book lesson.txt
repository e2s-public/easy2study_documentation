--------------------------------------------------------------------------------------
https://easy2study.com/war/api/course/personal/book_lesson/{id} - проверка для кнопки Book lesson;
Метод GET;
Вместо {id} ставим id учителя, например 9;
Требуется токен в header;
Если всё ок, то получим 200:
{
    "detail": "Access granted!"
}
Если нет, то 400:
{
    "detail": "You already have a booked time with this teacher. Please clear the list or pay!",
    "requests_link": "https://easy2study.com/war/api/course/personal/filter/?status=0&teacher=9" #ссылка на список неоплаченных заявок у учителя, нужен Token
}
Без токена - 401:
{
    "detail": "You must be authenticated!"
}
--------------------------------------------------------------------------------------
https://easy2study.com/war/api/course/personal/clean/{id} - очистка списка заявок на время учителя, ожидающих оплаты;
Метод GET;
Вместо {id} ставим id учителя, например 9;
Требуется токен в header;
В ответ получим 200:
{
    "detail": "List cleaned. Access granted!"
}
Без токена - 401:
{
    "detail": "You must be authenticated!"
}
--------------------------------------------------------------------------------------